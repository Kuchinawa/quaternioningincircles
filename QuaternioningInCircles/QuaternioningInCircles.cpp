// QuaternioningInCircles.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Quaternion.h"
#include <math.h>
#include <vector>
#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
    Quaternion rotation1;

    //90 degrees rotation around the x axis
    float angle = 90.0f;
    std::vector<float> axis = { 1.0f, 0.0f, 0.0f };

    rotation1.setToRotation(angle, axis);

    

    Matrix result = rotation1.toMatrix();

    std::cout << "Quaternion1: " << std::endl;
    
    std::cout << "Angle: " << angle << " Axis: " << axis.at(0) << ", " << axis.at(1) << ", " << axis.at(2) << std::endl;
    std::cout << "Normalized: " << rotation1.toString() << std::endl;
    std::cout << "Rotation Matrix: " << std::endl;
    std::cout << result.toString() << std::endl;

    std::cout << "--------------------------------" << std::endl;

    Quaternion rotation2(2, 3, -5, 1);
    Quaternion rotation3(-1, 2, 0, -1);


    std::cout << "Quaternion2: w =  2 x = 3, y = -5, z =  1" << std::endl;
    std::cout << "Quaternion3: w = -1 x = 2, y =  0, z = -1" << std::endl;
    
    std::cout << std::endl;
    
    std::cout << "Normalized: " << std::endl;
    std::cout << "Quaternion2: " << rotation2.toString() << std::endl;
    std::cout << "Quaternion3: " << rotation3.toString() << std::endl;

    std::cout << std::endl;

    std::cout << "Quaternion3 * Quaternion 2:" << std::endl;
    
    rotation2 = rotation3 * rotation2;

    std::cout << "Result: " << rotation2.toString() << std::endl;

    Matrix testMatrix = rotation2.toMatrix();

    std::cout << std::endl;

    std::cout << "Resulting Rotation Matrix: " << std::endl;
    std::cout << testMatrix.toString() << std::endl;

    std::cout << "Press ENTER to continue...";
    std::cin.sync();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');



    return 0;
}

