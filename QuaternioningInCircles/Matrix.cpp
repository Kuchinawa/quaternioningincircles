#include "stdafx.h"
#include "Matrix.h"

#include <string>
#include <sstream>

Matrix::Matrix(int rows, int columns, bool idMatrix)
{
    values.resize(rows);


    for (int row = 0; row < rows; row++)
    {
        for (int column = 0; column < columns; column++)
        {
            //If identity, set diagonal to 1
            if (row == column && idMatrix == true)
            {
                values.at(row).push_back(1.0f);
            }
            else
            {
                values.at(row).push_back(0.0f);
            }
        }
    }
}


Matrix::~Matrix()
{
}

float &Matrix::at(int row, int column)
{
    return values.at(row).at(column);
}

float Matrix::get(int row, int column) const
{
    return values.at(row).at(column);
}

int Matrix::rows() const
{
    return values.size();
}

int Matrix::columns() const
{
    return values.at(0).size();
}

void Matrix::deleteRow(const int r)
{
    values.erase(values.begin() + r);
}

void Matrix::deleteColumn(const int c)
{
    for(std::vector<float> &row : values)
    {
        row.erase(row.begin() + c);
    }
}

std::string Matrix::toString()
{
    std::ostringstream returnString;

    for (int r = 0; r < values.size(); r++)
    {
        for (int c = 0; c < values.at(r).size(); c++)
        {
            returnString << values.at(r).at(c) << ", " ;
        }
        returnString << std::endl;
    }

    return std::string(returnString.str());
}

Matrix& Matrix::operator=(const Matrix& m)
{
    values = m.values;
    return *this;
}

Matrix& Matrix::operator*(const Matrix& m)
{
    Matrix matrix(rows(), m.columns(), false);

    for (int r = 0; r < rows(); r++)
    {
        for (int c = 0; c < m.columns(); c++)
        {
            for (int i = 0; i < m.rows(); i++)
            {
                matrix.at(r, c) += values.at(r).at(i) * m.get(i, c);
            }
        }
    }

    return matrix;
}

Matrix& Matrix::operator*=(const Matrix& m)
{
    for (int r = 0; r < rows(); r++)
    {
        for (int c = 0; c < m.columns(); c++)
        {
            for (int i = 0; i < m.rows(); i++)
            {
                at(r, c) += values.at(r).at(i) * m.get(i, c);
            }
        }
    }

    return *this;
}

Matrix& Matrix::operator*(const float& value)
{
    for (int r = 0; r < rows(); r++)
    {
        for (int c = 0; c < columns(); c++)
        {
            at(r, c) = values.at(r).at(c) * value;
        }
    }

    return *this;
}

Matrix& Matrix::operator+(const Matrix& m)
{
    Matrix matrix(rows(), columns());

    for (int r = 0; r < rows(); r++)
    {
        for (int c = 0; c < m.columns(); c++)
        {
            matrix.at(r, c) = get(r, c) + m.get(r, c);
        }
    }

    return matrix;
}

Matrix& Matrix::operator+=(const Matrix& m)
{
    for (int r = 0; r < rows(); r++)
    {
        for (int c = 0; c < m.columns(); c++)
        {
            at(r, c) += m.get(r, c);
        }
    }

    return *this;
}

Matrix& Matrix::operator-(const Matrix& m)
{
    Matrix matrix(rows(), columns());

    for (int r = 0; r < rows(); r++)
    {
        for (int c = 0; c < m.columns(); c++)
        {
            matrix.at(r, c) = get(r, c) - m.get(r, c);
        }
    }

    return matrix;
}

Matrix& Matrix::operator-=(const Matrix& m)
{
    for (int r = 0; r < rows(); r++)
    {
        for (int c = 0; c < m.columns(); c++)
        {
            at(r, c) -= m.get(r, c);
        }
    }

    return *this;
}