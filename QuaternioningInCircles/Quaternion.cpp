#include "stdafx.h"
#include "Quaternion.h"

#include <math.h>
#include <sstream>

Quaternion::Quaternion()
{
    this->w = 1;
    this->x = 0;
    this->y = 0;
    this->z = 0;
}

Quaternion::Quaternion(float w, float x, float y, float z, bool normalize)
{
    this->w = w;
    this->x = x;
    this->y = y;
    this->z = z;

    if (normalize)
    {
        this->normalize();
    }
}


Quaternion::~Quaternion()
{
}

//Careful! Q1 * Q2 != Q2 * Q1 !!!
Quaternion& Quaternion::operator*(const Quaternion& q)
{
    Quaternion result;
    result.w = w * q.w - x * q.x - y * q.y - z * q.z;
    result.x = w * q.x + x * q.w + y * q.z - z * q.y;
    result.y = w * q.y - x * q.z + y * q.w + z * q.x;
    result.z = w * q.z + x * q.y - y * q.x + z * q.w;

    return result;
}

void Quaternion::normalize()
{
    float magnitude = sqrtf(powf(w, 2) + powf(x, 2) + powf(y, 2) + powf(z, 2));
    w /= magnitude;
    x /= magnitude;
    y /= magnitude;
    z /= magnitude;
}

//Set the quaternion to a specific rotation
void Quaternion::setToRotation(const float angle, const std::vector<float> axis)
{
    w = cosf(angle / 2);
    x = axis.at(0) * sinf(angle / 2);
    y = axis.at(1) * sinf(angle / 2);
    z = axis.at(2) * sinf(angle / 2);
}

Matrix Quaternion::toMatrix() const
{
    Matrix result(4, 4);

    result.at(0, 0) = w*w + x*x - y * y - z*z;
    result.at(0, 1) = 2 * x * y - 2 * w * z;
    result.at(0, 2) = 2 * x * z + 2 * w * y;

    result.at(1, 0) = 2 * x * y + 2 * w * z;
    result.at(1, 1) = w * w - x * x + y * y - z * z;
    result.at(1, 2) = 2 * y * z - 2 * w * x;

    result.at(2, 0) = 2 * x * z - 2 * w * y;
    result.at(2, 1) = 2 * y * z + 2 * w * x;
    result.at(2, 2) = w * w - x * x - y * y + z * z;

    return result;
}

std::string Quaternion::toString()
{
    std::ostringstream returnString;

    returnString << "w = " << w << " x = " << x << " y = " << y << " z = " << z;

    return std::string(returnString.str());
}