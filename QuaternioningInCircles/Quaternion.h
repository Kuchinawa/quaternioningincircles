#pragma once

#include <vector>
#include "Matrix.h"

class Quaternion
{
public:
    Quaternion();
    ~Quaternion();

    Quaternion(float w, float x, float y, float z, bool normalize = true);

    void normalize();

    std::string toString();

    //Careful! Q1 * Q2 != Q2 * Q1 !!!
    Quaternion& operator*(const Quaternion& q);

    //Set the quaternion to a specific rotation
    void setToRotation(const float angle, const std::vector<float> axis);

    Matrix toMatrix() const;

    float w;

    float x;
    float y;
    float z;

private:

};

